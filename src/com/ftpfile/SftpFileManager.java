package com.ftpfile;

import java.io.File;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;
import java.util.Vector;

import org.slf4j.LoggerFactory;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.ChannelSftp.LsEntry;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;

public class SftpFileManager extends RemoteFileManagment{
	private Session session = null;
	private ChannelSftp channel = null;
	
	public SftpFileManager(FtpConfig ftpconfig, FtpDatasourceConfig dataSourceConfig) {
		super();
		this.ftpconfig = ftpconfig;
		this.dataSourceConfig = dataSourceConfig;
		//this.dirPatternFiler = new DirPatternFilterDate(this.dataSourceConfig);
		logger = LoggerFactory.getLogger(SftpFileManager.class);
	}

	@Override
	public boolean login() {
		// TODO Auto-generated method stub
		if( null == ftpconfig ) {
			logger.error("SftpFileManager:ftpconfig is null.");
			return false;
		}
		
		try {
	        JSch jsch = new JSch();
	        /*Construct session.*/
	        if( 0 == ftpconfig.getFtpServerPort() ){
	        	session = jsch.getSession(ftpconfig.getUser(), ftpconfig.getFtpServerIp());
	        }else {
	        	session = jsch.getSession(ftpconfig.getUser(), ftpconfig.getFtpServerIp(), ftpconfig.getFtpServerPort());
	        }
	        
	        if(ftpconfig.getPassword() != null){
	            session.setPassword(ftpconfig.getPassword());
	        }
	        Properties config = new Properties();
	        config.put("StrictHostKeyChecking", "no");
	        session.setConfig(config);
	        session.setTimeout(timeout);
	        session.connect();
	        logger.info("sftp session connected");
	        /*Open channel*/
	        logger.info("opening channel");
	        channel = (ChannelSftp)session.openChannel("sftp");
	        channel.connect();
	        
	        logger.info("connected successfully");
	        return true;
	    } catch (JSchException e) {
	        logger.error("sftp login failed",e);
	        return false;
	    }
	}

	@Override
	public void logout() {
		// TODO Auto-generated method stub
		if(channel != null){
		    channel.quit();
		    channel.disconnect();
		}
		if(session != null){
		    session.disconnect();
		}
		logger.info("logout successfully");
	}

	/**
	 * Load all files and directories from remote to local,just copy the directories.
	 * @param filteredDir
	 * @return filteredDir
	 */
	protected String loadAllFromRemote(String filteredDir) {
		/*If filteredDir(local) is not existed locally then create it.*/
		File dir = new File(filteredDir);
	    if( !dir.exists() ){
	    	dir.mkdirs();
	    }
	    
	    try {
			channel.lcd(filteredDir);
			lsFolderCopy(filteredDir,filteredDir);
		} catch (SftpException e) {
			// TODO Auto-generated catch block
			logger.error("loadAllFromRemote error!",e);
		}
	    
	    return filteredDir;
	}
	
	/**
	 * Find the remote directory according to given pattern.
	 * @return
	 */
	protected String filterDir() {
		String filteredDir = null;
	    if(!changeDir(dataSourceConfig.getRemoteFilesDirectory())){  /*switch to remote root working directory.*/
	        return null;
	    }else {
	    	filteredDir = filterDirViaPattern(currentDir());
	    }
	    
	    return filteredDir;
	}
	
	/**Get files via fileFilterRegex,add them to list
	 * @param dirPath
	 * @return 
	 */
	protected List<File> getSpecifiedFiles(String dirPath) {
		List<File> files = new LinkedList<File>();
		String[] fileNames = null;
		
		try {
			channel.lcd(dirPath);

			File dir = new File(dirPath);
			if( dir.isDirectory() ){
				fileNames = dir.list();
				if( null != fileNames && 0 != fileNames.length ) {
					fileFilter = new FileFilter(dataSourceConfig.getFileFilterRegex());  /* ".*?(zip)" */
					for( String fileName:fileNames ){
						File file = new File(dirPath+File.separator+fileName);
	    				if( true == fileFilter.accept(file, fileName) ) {
	    			        if(!file.exists()){
	    			            logger.info("filename=["+fileName+"] is not found under dir=["+dirPath+"].");
	    			        }else {
	    			        	files.add(file);
	    			        }
	    				}
					}
				}
			}else {
				logger.error("dirPath is not a directory. dirPath = "+dirPath);
			}
		} catch (SftpException e) {
			// TODO Auto-generated catch block
			logger.error("Switch to local directory ["+dirPath+"] failed.",e);
		}
		
		return files;
	}
	
	@Override
	public List<File> getFiles() {
		// TODO Auto-generated method stub
		String filteredDir = loadAllFromRemote(filterDir());
		if( null == filteredDir ) {
			return null;
		}
	    return getSpecifiedFiles(filteredDir);
	}

	@Override
	public void putFiles(List<File> files) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void compressFiles(List<File> files) {
		// TODO Auto-generated method stub
		
	}

	
	
	@Override
	public boolean decompressFiles(List<File> files) {
		// TODO Auto-generated method stub
		boolean result = false;
		if( null != files && 0 != files.size() ){
			String outputDir = dataSourceConfig.getLocalFilesDirectory();
			for( File file:files ){
				result = unzipFile(file,outputDir); /*All files must be OK.*/
				if( false == result ){
					return result;
				}
			}
		}else {
			logger.info("decompressFiles:No files to decompress!");;
		}
		/*Files will be decompressed and put to 
		 * (localFilesDirectory + secondaryDirFilterPattern)*/
		return result;
	}

	protected String homeDir(){
	    try {
	        return channel.getHome();
	    } catch (SftpException e) {
	        return "/";
	    }
	}
	
	protected String currentDir(){
	    try {
	        return channel.pwd();
	    } catch (SftpException e) {
	        logger.error("failed to get current dir",e);
	        return homeDir();
	    }
	}
	
	/**
	 * Switch to the specified path
	 * @param pathName
	 * @return
	 */
	protected boolean changeDir(String pathName){
	    if(pathName == null || pathName.trim().equals("")){
	        logger.info("invalid pathName");
	        return false;
	    }
	    
	    try {
	        channel.cd(pathName.replaceAll("\\\\", "/"));
	        logger.info("directory successfully changed,current dir=" + channel.pwd());
	        return true;
	    } catch (SftpException e) {
	        logger.error("failed to change directory",e);
	        return false;
	    }
	}
	
	
	/**
	 * Find the directory specified by directoryFilterPattern or directoryFilterRegex in  FtpConfig and return its name
	 * @param currentPathName
	 * @return name of the directory which is filtered by the given condition 
	 */
	protected String filterDirViaPattern(String currentPathName) {
		try {
			Vector<LsEntry> vec = channel.ls(currentPathName); /*remote root directory*/
			if( null != vec ){
				Iterator<LsEntry> ite = vec.iterator();
				while( ite.hasNext() ){
					LsEntry entry = ite.next();
					String firstLevel = entry.getFilename();  /*first level directory*/
					if( dirPatternFiler.accept(firstLevel) ) {
						channel.cd(firstLevel);
					}else{
						continue;
					}
					
					/*first level directory*/
					String absolutePath = currentPathName+File.separator+firstLevel;
					/*second level directory*/
					if( null != dataSourceConfig.getSecondaryDirFilterPattern() ) {
						String secondLevel = dataSourceConfig.getSecondaryDirFilterPattern();
						absolutePath += (File.separator+secondLevel);
						channel.cd(secondLevel);
					}
					
					logger.info("filterDirViaPattern:Find the specified directory ="+absolutePath);
					return absolutePath;
				}
			}
			
			logger.info("filterDirViaPattern:Directory not found,pattern=["+dataSourceConfig.getPrimaryDirFilterPattern()+"],cur dir=["+currentPathName+"].");
		} catch (SftpException e) {
			// TODO Auto-generated catch block
			logger.info("filterDirViaPattern failed!",e);
		}
	
		return null;
	}
	
	/**
	 * Copy all files and directories under sourcePath to local destPath
	 * @param sourcePath
	 * @param destPath
	 * @throws SftpException
	 */
	protected void lsFolderCopy(String sourcePath, String destPath) { 
		// List source (remote, sftp) directory and create a local copy of it - method for every single directory.
	    Vector<ChannelSftp.LsEntry> list = null;
		try {
			list = channel.ls(sourcePath);
			// List source directory structure.
		    for (ChannelSftp.LsEntry oListItem : list) { 
		    	// Iterate objects in the list to get file/folder names.
		        if (!oListItem.getAttrs().isDir()) { 
		        	// If it is a file (not a directory).
		            if (!(new File(destPath + File.separator + oListItem.getFilename())).exists() 
		            	|| (oListItem.getAttrs().getMTime() > Long.valueOf(new File(destPath + File.separator + oListItem.getFilename()).lastModified() / (long) 1000).intValue())) { 
		            	// Download only if changed later.
		                //new File(destPath + "/" + oListItem.getFilename());
		                channel.get(sourcePath + File.separator + oListItem.getFilename(), destPath + File.separator + oListItem.getFilename()); // Grab file from source ([source filename], [destination filename]).
		            }
		        } else if ( false == ".".equals(oListItem.getFilename()) 
		        		 && false == "..".equals(oListItem.getFilename()) ){
		            new File(destPath + File.separator + oListItem.getFilename()).mkdirs(); // Empty folder copy.
		            lsFolderCopy(sourcePath + File.separator + oListItem.getFilename(), destPath + File.separator + oListItem.getFilename()); // Enter found folder on server to read its contents and create locally.
		        }
		    }
		} catch (SftpException e) {
			// TODO Auto-generated catch block
			logger.error("lsFolderCopy failed!",e);
		} 
	}
}
