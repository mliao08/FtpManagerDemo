package com.ftpfile;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class DirPatternFilterDate implements DirPatternFilter{
	private FtpDatasourceConfig dataSourceConfig = null;
	private String dateString = null;
	private static Logger logger = LoggerFactory.getLogger(DirPatternFilterDate.class);
	
	public DirPatternFilterDate() {
		super();
	}

	@Override
	public boolean accept(String name) {
		// TODO Auto-generated method stub
		if( null != dateString && name.contains(dateString) ){
			return true;
		}
		
		return false;
	}

	@Override
	public DirPatternFilter setConfig(FtpDatasourceConfig dsConfig) {
		// TODO Auto-generated method stub
		this.dataSourceConfig = dsConfig;
		return this;
	}

	@Override
	public boolean init() {
		// TODO Auto-generated method stub
		if( null != dataSourceConfig ){
			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat(this.dataSourceConfig.getPrimaryDirFilterPattern(),Locale.ENGLISH);
			dateString = sdf.format(date);
			
			logger.debug("directory filter pattern="+dateString);
			return true;
		}
		
		return false;
	}

}
