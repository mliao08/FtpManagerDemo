package com.ftpfile;

public interface RemoteFileHandler {
	public void handle();
	public void setDatasourceConfig(FtpDatasourceConfig dsConfig);
}
