package com.ftpfile;

import java.io.File;
import java.io.FilenameFilter;
import java.util.regex.Pattern;


public class FileFilter implements FilenameFilter{
	private Pattern pattern = null;
	
	public FileFilter(String regexPattern) {
		pattern = Pattern.compile(regexPattern);
	}
	
	
	@Override
	public boolean accept(File dir, String name) {
		// TODO Auto-generated method stub
		return pattern.matcher(name).matches();
	}

}
