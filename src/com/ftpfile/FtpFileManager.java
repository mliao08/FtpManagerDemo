package com.ftpfile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.SocketException;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPReply;
import org.slf4j.LoggerFactory;

public class FtpFileManager extends RemoteFileManagment
{
	private FTPClient client = null;
	
	public FtpFileManager(FtpConfig ftpconfig, FtpDatasourceConfig dataSourceConfig) {
		super();
		this.ftpconfig = ftpconfig;
		this.dataSourceConfig = dataSourceConfig;
		//this.dirPatternFiler = new DirPatternFilterDate(this.dataSourceConfig);
		logger = LoggerFactory.getLogger(FtpFileManager.class);
	} 
	
	@Override
	public boolean login() {
		// TODO Auto-generated method stub
		client = new FTPClient();  
        client.setConnectTimeout(timeout);  
        try {  
            /*Connect to remote FTP server*/
            if(!client.isConnected()){  
            	/*default control port is 21,if the port isn't specified,then use default connection.*/
            	if( 0 == ftpconfig.getFtpServerPort() ) {
            		client.connect(ftpconfig.getFtpServerIp());
            	}else {
            		client.connect(ftpconfig.getFtpServerIp(), ftpconfig.getFtpServerPort());
            	}
                
            	System.out.println(client.getReplyString()); /*show reply message.for test*/
            	logger.info(client.getReplyString());
            	/*check connection status*/
            	int reply = 0;
            	reply = client.getReplyCode();
                if(!FTPReply.isPositiveCompletion(reply)) {
                  client.disconnect();
                  System.out.println("FTP server refused connection.");
                  logger.error("FTP server refused connection. replyCode="+reply);
                  return false;
                }
            	
                /*login*/
                client.login(ftpconfig.getUser(), ftpconfig.getPassword());  
                reply = client.getReplyCode();  
                
                /*check login status*/
                if (!FTPReply.isPositiveCompletion(reply)) {  
                    logger.info("Fail to connect remote FTP server,invalid username or password.");  
                    client.disconnect();  
                    throw new RuntimeException("Fail to connect remote FTP server,invalid username or password.");  
                } else {  
                    logger.info("Success to connect FTP server.IP:"+ftpconfig.getFtpServerIp() +"PORT:" +ftpconfig.getFtpServerPort());  
                }  
                
                client.setControlEncoding("UTF-8");  
                client.setFileType(FTPClient.BINARY_FILE_TYPE);    
                client.enterLocalPassiveMode();  
            }  
        } catch (SocketException e) {  
            try {  
                client.disconnect();  
            } catch (IOException e1) {  
            }  
            logger.error("Fail to connect remote FTP server." + e.getMessage());  
            throw new RuntimeException("Fail to connect remote FTP server." + e.getMessage());  
        } catch (IOException e) {  
        }  
        return true;  
	}

	@Override
	public void logout() {
		// TODO Auto-generated method stub
		if( null != client ){
			try {  
	            client.disconnect();  
	            logger.info(" Shutting down FTP connection ! ");  
	        } catch (IOException e) {  
	            logger.warn(" Fail to shutdown FTP connection ! ",e);  
	        }  
		}
	}

	protected String homeDir(){
		return "/";
	}
	
	/**
	 * Switch to the specified path
	 * @param pathName
	 * @return
	 */
	protected boolean changeDir(String pathName){
	    if(pathName == null || pathName.trim().equals("")){
	        logger.info("invalid pathName");
	        return false;
	    }
	    
	    try {
	    	String currentDir = pathName.replaceAll("\\\\", "/");
	    	new File(currentDir).mkdirs();
	    	/*chroot_local_user in /etc/vsftpd/vsftpf.conf must be set to NO,or,users 
	    	 * login cannot change their working directories.*/
			if(client.changeWorkingDirectory(currentDir)){
				logger.info("directory successfully changed,current dir=" + client.printWorkingDirectory());
				return true;
			}else {
				logger.info("change working directory failed! replycode="+client.getReplyCode()+", replyMessage="+client.getReplyString());
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.error("Fail to switch to dir="+pathName,e);
		}
	    
	    return false;
	}
	
	protected String currentDir(){
		try {
			return client.printWorkingDirectory();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.error("Fail to get current working directory!",e);
			return homeDir();
		}
	}
	
	/**
	 * Find the directory specified by directoryFilterPattern or directoryFilterRegex in  FtpConfig and return its name
	 * @param currentPathName
	 * @return name of the directory which is filtered by the given condition 
	 */
	protected String filterDirViaPattern(String currentPathName) {
		String absolutePath = null;
		FTPFile[] ftpFiles = null;
		try {
			ftpFiles = client.listFiles(currentPathName);
			if( null != ftpFiles ){
				for( FTPFile file:ftpFiles ){
					String firstLevel = file.getName(); /*first level directory filter*/
					if( dirPatternFiler.accept(firstLevel) ) {
						absolutePath = currentPathName+File.separator+firstLevel;
						client.changeWorkingDirectory(absolutePath);
					}else{
						continue;
					}
					
					if( null != dataSourceConfig.getSecondaryDirFilterPattern() ) {
						String secondLevel = dataSourceConfig.getSecondaryDirFilterPattern();
						absolutePath += (File.separator+secondLevel);
						new File(absolutePath).mkdirs();
						if(!client.changeWorkingDirectory(absolutePath)){
							int replyCode = client.getReplyCode();
							logger.info("Change working directory to ["+absolutePath+"] failed,replycode="+replyCode+", message="+client.getReplyString());
							return null;
						}
					}
					
					logger.info("filterDirViaPattern:Find the specified directory ="+absolutePath);
					return absolutePath;
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.error("filterDirViaPattern failed!",e);
		} 
		
		logger.info("filterDirViaPattern:Directory not found,pattern=["+dataSourceConfig.getPrimaryDirFilterPattern()+"],cur dir=["+currentPathName+"].");
		return null;
	}
	
	/**
	 * Find the remote directory according to given pattern.
	 * @return
	 */
	protected String filterDir() {
		String filteredDir = null;
	    if(!changeDir(dataSourceConfig.getRemoteFilesDirectory())){  /*switch to remote root working directory.*/
	        return null;
	    }else {
	    	filteredDir = filterDirViaPattern(currentDir());
	    }
	    
	    return filteredDir;
	}
	
	private void getFileViaFtp(FTPFile srcFile, String dstPath) {
		InputStream is = null;
		try {
			is = client.retrieveFileStream(srcFile.getName());
			if( null == is ) {
	        	logger.error("Fail to download,check if the file exists. file="+srcFile.getName());
	        }
	        
	        File downloadFile = new File(dstPath + File.separator + srcFile.getName());  
	        FileOutputStream fos = FileUtils.openOutputStream(downloadFile);  
	        IOUtils.copy(is, fos);  
	        client.completePendingCommand();  
	        IOUtils.closeQuietly(is);  
	        IOUtils.closeQuietly(fos);  
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.error("getFile failed!",e);;
		}  
	}
	
	/**
	 * Copy all files and directories under sourcePath to local destPath
	 * @param sourcePath
	 * @param destPath
	 * @throws SftpException
	 */
	protected void lsFolderCopy(String sourcePath, String destPath) { 
		// List source (remote, sftp) directory and create a local copy of it - method for every single directory.
	    FTPFile[] list = null;
		try {
			list = client.listFiles(sourcePath);
			for( FTPFile ftpFile:list ){
		    	// If it is a file (not a directory).
		    	if( ftpFile.isFile() ){
		    		if(!(new File(destPath + File.separator + ftpFile.getName())).exists()  /*This file isn't existed in local directory.*/
			           || (ftpFile.getTimestamp().getTimeInMillis() > new File(destPath + File.separator + ftpFile.getName()).lastModified())) {
		    			
		    			getFileViaFtp(ftpFile,destPath);
		    		}
		    	}else if( false == ".".equals(ftpFile.getName()) 
		        		 && false == "..".equals(ftpFile.getName()) ) {
		    		//If it is a directory
		    		new File(destPath + File.separator + ftpFile.getName()).mkdirs(); // Empty folder copy.
		            lsFolderCopy(sourcePath + File.separator + ftpFile.getName(), destPath + File.separator + ftpFile.getName()); // Enter found folder on server to read its contents and create locally.
		    	}
		    }
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.error("lsFolderCopy failed!",e);;
		}
	}
	
	/**
	 * Load all files and directories from remote to local,just copy the directories.
	 * @param filteredDir
	 * @return filteredDir
	 */
	protected String loadAllFromRemote(String filteredDir) {
		/*If filteredDir(local) is not existed locally then create it.*/
		File dir = new File(filteredDir);
	    if( !dir.exists() ){
	    	dir.mkdirs();
	    }
	    
	    lsFolderCopy(filteredDir,filteredDir);
	    
	    return filteredDir;
	}
	
	/**Get files via fileFilterRegex,add them to list
	 * @param dirPath
	 * @return 
	 */
	protected List<File> getSpecifiedFiles(String dirPath) {
		List<File> files = new LinkedList<File>();
		String[] fileNames = null;
		
		File dir = new File(dirPath);
		if( dir.isDirectory() ){
			fileNames = dir.list();
			if( null != fileNames && 0 != fileNames.length ) {
				fileFilter = new FileFilter(dataSourceConfig.getFileFilterRegex());  /* ".*?(zip)" */
				for( String fileName:fileNames ){
					File file = new File(dirPath+File.separator+fileName);
    				if( true == fileFilter.accept(file, fileName) ) {
    			        if(!file.exists()){
    			            logger.info("filename=["+fileName+"] is not found under dir=["+dirPath+"].");
    			        }else {
    			        	files.add(file);
    			        }
    				}
				}
			}
		}else {
			logger.error("dirPath is not a directory. dirPath = "+dirPath);
		}
		
		return files;
	}
	
	@Override
	public List<File> getFiles() {
		// TODO Auto-generated method stub
		String filteredDir = loadAllFromRemote(filterDir());
		if( null == filteredDir ) {
			return null;
		}
		
	    return getSpecifiedFiles(filteredDir);
	}

	@Override
	public void putFiles(List<File> files) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void compressFiles(List<File> files) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean decompressFiles(List<File> files) {
		// TODO Auto-generated method stub
		boolean result = false;
		if( null != files && 0 != files.size() ){
			String outputDir = dataSourceConfig.getLocalFilesDirectory();
			for( File file:files ){
				result = unzipFile(file,outputDir);
				if( false == result ){
					return result;
				}
			}
		}else {
			logger.info("decompressFiles:No files to decompress!");;
		}
		
		return result;
	}

}
