package com.ftpfile;

public interface DirPatternFilter {
	public boolean accept(String name);
	public boolean init();
	public DirPatternFilter setConfig(FtpDatasourceConfig dsConfig);
}
