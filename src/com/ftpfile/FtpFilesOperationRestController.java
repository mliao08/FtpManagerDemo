package com.ftpfile;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FtpFilesOperationRestController {
	
	public static void main(String[] args) {
		SpringApplication.run(FtpFilesOperationRestController.class, args);
	}
}
