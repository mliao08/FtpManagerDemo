package com.ftpfile;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value="/api/v1")
public class FileManagerRestController {
	
	private FtpConfig ftpconfig;
	private RemoteFileManagment fileManager;
	@Autowired
	private DirPatternFilter dirPatternFiler;    /*User define*/
	@Autowired
	private RemoteFileHandler remoteFileHanaler; /*User define*/
	
	
	private static Logger logger = LoggerFactory.getLogger(FileManagerRestController.class);
	
	@RequestMapping(value="/getRemoteFiles",method=RequestMethod.POST)
	public String launchTask(@RequestBody FtpConfig reqFtpConfig,HttpServletRequest request,HttpServletResponse response) {
		if( null == reqFtpConfig ) { 
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			return "Get FTP config from request body failed!"; 
		}
		
		ftpconfig = reqFtpConfig;
		
		/*Setup a relative timer task,or launch the task instantly if
		 * ftpconfig.dataSyncTime equals "now"*/
		if( null != ftpconfig.getDataSourceConfig() ) {
			List<FtpDatasourceConfig> dataSrcConfigs = ftpconfig.getDataSourceConfig();
			
			for( int i=0; i < dataSrcConfigs.size(); i++ ){
				if( null != dataSrcConfigs.get(i).getDataSyncTime() && dataSrcConfigs.get(i).getDataSyncTime().equalsIgnoreCase("now")) {
					if( ftpconfig.getProtocol().equalsIgnoreCase("sftp") ){
						fileManager = new SftpFileManager(ftpconfig,dataSrcConfigs.get(i));
					}else if( ftpconfig.getProtocol().equalsIgnoreCase("ftp") ){
						fileManager = new FtpFileManager(ftpconfig,dataSrcConfigs.get(i));
					}else {
						logger.error("Unsupported protocol!");
						return "ERROR:Unsupported protocol!";
					}
					
					if( null != fileManager ){
						if( fileManager.login() ){
							boolean result = fileManager.decompressFiles(fileManager.setDirFilter(dirPatternFiler).getFiles());
							fileManager.removeLocalZipFiles();
							fileManager.logout();
							if( true == result ){
								fileManager.setRemoteFileHandler(remoteFileHanaler).handleRemoteFile();
							}							
						}
					}else {
						logger.error("filesManager is null!");
						return "ERROR:fileManager is null!";
					}
				}
			}
		}
		
		return "Success!";
	}
}
