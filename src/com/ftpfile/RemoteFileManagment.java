package com.ftpfile;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.slf4j.Logger;

public abstract class RemoteFileManagment {
	protected Logger logger;
	protected FileFilter fileFilter;
	protected FtpConfig ftpconfig;
	protected FtpDatasourceConfig dataSourceConfig;
	protected DirPatternFilter dirPatternFiler;
	protected int timeout = 60000; /*60 sec*/
	protected RemoteFileHandler remoteFileHanaler;
	
	public void removeLocalZipFiles() {
		removeLocalDir(dataSourceConfig.getRemoteFilesDirectory());
	}
	
	protected void removeLocalDir(String dirPath) {
		File localDir = new File(dirPath);
		if( true == localDir.exists() && true == localDir.isDirectory() ){
			deleteAll(localDir);
		}else {
			logger.info("removeLocalDir:"+dirPath+" is not a directory or not exist.");
		}
	}
	
	/**
	 * Unzip the file to a specified directory
	 * @param file
	 * @param outputDir
	 */
	protected boolean unzipFile(File file, String outputDir) {
		byte[] buffer = new byte[1024*1024];

		File folder = new File(outputDir);
		if( false == folder.exists() ){
			folder.mkdirs();
		}
		
		ZipInputStream zis = null;
		try {
			zis = new ZipInputStream(new FileInputStream(file));
			ZipEntry ze = zis.getNextEntry();
			
			while(ze!=null){
				/*Create target directory*/
				String identyfierDir = null;
				String parentPath = file.getParent();
		        if( null != parentPath ) {
		        	String[] paths = parentPath.split(File.separator);
		        	if( null != paths && paths.length > 0) {
		        		identyfierDir = paths[paths.length-1];
		        	}
		        }
		        
		        String zipFileName = file.getName();
		        String[] zipFileNameSplit = zipFileName.split("\\.");
		        String zipFileNameNew = null;
		        if( null != zipFileNameSplit && zipFileNameSplit.length > 0) {
		        	zipFileNameNew = zipFileNameSplit[0];
		        }
		        
		        String dstFilePath = null;
		        if( null == identyfierDir ) {
		        	dstFilePath = outputDir+File.separator+zipFileNameNew; /*use zip file name as identify directory*/
		        }else {
		        	dstFilePath = outputDir+File.separator+identyfierDir+File.separator+zipFileNameNew;
		        }
		        
		        new File(dstFilePath).mkdirs();  
		        logger.debug("file unzip to : "+ dstFilePath);
		        
		        
				
				/*Create target file*/
		        String fileName = ze.getName();  /*Name of the file in the zip-format package.*/
		        File newFile = new File(dstFilePath + File.separator + fileName); /*target file*/
		        logger.debug("file unzip : "+ newFile.getAbsoluteFile());
		        
				/*Write to target file*/
		        FileOutputStream fos = new FileOutputStream(newFile);
		        int len;
	            while ((len = zis.read(buffer)) > 0) {
	       		fos.write(buffer, 0, len);
	            }

	            fos.close();
	            ze = zis.getNextEntry();    
		    }
			
			return true;
		} catch(FileNotFoundException e){
			logger.error("unzipFile",e);
			return false;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.error("unzipFile",e);
			return false;
		} finally {
			if( null != zis ) {
				try {
					zis.closeEntry();
					zis.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					logger.error("unzipFile",e);
					return false;
				}
			}
		}
	}
	
	protected void deleteAll(File file) {
		/*Traverse all files and directory,delete all*/
		if( true == file.isFile() ){
			file.delete();
		}else {
			File[] files = file.listFiles();
			for( File fileItem:files ){
				deleteAll(fileItem);
				if( fileItem.exists() ){
					fileItem.delete();
				}
			}
		}
	}
	
	public RemoteFileManagment setDirFilter(DirPatternFilter dirPatternFiler){
		this.dirPatternFiler = dirPatternFiler;
		if( null != this.dirPatternFiler ){
			dirPatternFiler.setConfig(dataSourceConfig).init();
		}
		return this;
	}
	
	public RemoteFileManagment setRemoteFileHandler(RemoteFileHandler handler) {
		remoteFileHanaler = handler;
		remoteFileHanaler.setDatasourceConfig(dataSourceConfig);
		return this;
	}
	
	public void handleRemoteFile() {
		if( null != remoteFileHanaler ){
			remoteFileHanaler.handle();
		}else {
			logger.error("Handler is null,nothing can be done!");
		}
	}
	
	/*public abstract methods*/
	public abstract boolean login();
	public abstract void logout();
	public abstract List<File> getFiles();  /*Get files from specified path*/
	public abstract void putFiles(List<File> files);
	public abstract void compressFiles(List<File> files);
	public abstract boolean decompressFiles(List<File> files);
	
	/*non-public abstract methods,implicit operations should be declared here,
	 * use these declarations as a template.*/
	protected abstract String loadAllFromRemote(String filteredDir);
	protected abstract String homeDir();
	protected abstract boolean changeDir(String pathName);
	protected abstract String currentDir();
	protected abstract String filterDirViaPattern(String currentPathName);
	protected abstract String filterDir();
	protected abstract void lsFolderCopy(String sourcePath, String destPath);
	protected abstract List<File> getSpecifiedFiles(String dirPath);
}
