package com.ftpfile;

import java.util.List;

import org.springframework.stereotype.Component;

@Component
public class FtpConfig {
	/*Ftp server configuration*/
	private boolean isEnable;
	private String protocol;   /*"ftp"/"sftp"*/
	private String ftpServerIp;
	private int ftpServerPort;
	private String user;
	private String password;
	
	/*Date-source configuration*/	
	private List<FtpDatasourceConfig> dataSourceConfig;

	public boolean isEnable() {
		return isEnable;
	}

	public void setEnable(boolean isEnable) {
		this.isEnable = isEnable;
	}

	public String getProtocol() {
		return protocol;
	}

	public void setProtocol(String protocol) {
		this.protocol = protocol;
	}

	public String getFtpServerIp() {
		return ftpServerIp;
	}

	public void setFtpServerIp(String ftpServerIp) {
		this.ftpServerIp = ftpServerIp;
	}

	public int getFtpServerPort() {
		return ftpServerPort;
	}

	public void setFtpServerPort(int ftpServerPort) {
		this.ftpServerPort = ftpServerPort;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public List<FtpDatasourceConfig> getDataSourceConfig() {
		return dataSourceConfig;
	}

	public void setDataSourceConfig(List<FtpDatasourceConfig> dataSourceConfig) {
		this.dataSourceConfig = dataSourceConfig;
	}

}
